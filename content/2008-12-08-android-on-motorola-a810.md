+++
title = "Android on Motorola A810"
aliases = ["2008/12/android-on-motorola-a810.html"]
date = "2008-12-08T17:41:00Z"
[taxonomies]
tags = ["a810", "Android", "E28", "E2831", "Motorola", "neuf", "omap730", "platforms"]
categories = ["software"]
authors = ["peter"]
+++
First, let's have a look at the videos:

* Jaing 27 on YouTube: [Google Android on Motorola A810 - Android phone](https://www.youtube.com/watch?v=kYtD_gDtVPU)
* Jaing 27 on YouTube: [Google Android on Motorola A810 & Dailing 10086 (service number of china mibile)](https://www.youtube.com/watch?v=1jX5R6XDPLE)

So, does this mean, ANDROID on EZX is possible right now, with phone functions working? No, even if the A810 is still listed as an EZX 2nd generation device in the <a href="http://wiki.openezx.org/">OpenEZX Wiki</a>, this isn't the truth. It is in fact, as I've been knowing about for a long time&mdash;but never wrote here –, a phone made by china based <a href="http://www.e28.com/">E28 Inc.</a> (maybe you remember them as the manufacturer of the <a href="http://www.opentwin.org/">NEUF Twin tact</a>), and not based on a PXA270/Freescale Neptune solution, but on OMAP730/850 (<a href="https://linmob.net/android-runs-on">like the Twin Tact, which&mdash;branded as E28 E2831&mdash;was the first real ready-to-market phone which ANDROID was ever demoed on</a>).

Unfortunately the guy who posted these videos and <a href="http://www.motorolafans.com/forums/android-os/24201-android-motorola-a810.html">embedded them on Motorolafans</a> isn't the guy who shot them - so there aren't any explanations yet.
But this should kick the porting of ANDROID to the E28 E2831, anyway, because this in fact shows that it should be possible to skilled people everyone to do such a port.

After finding these videos, I searched for explanations on how to get Android running on E28 devices - I didn't find THE explanation, but only some hints.

I will share the links without much comment, if you're interested in this topic you have to read them, anyway:

* <a href="http://www.mail-archive.com/android-internals@googlegroups.com/msg00616.html">Android 0.9 on OMAP730 - Android internals</a>,
* <a href="http://vivien.chappelier.free.fr/typhoon/index.html">Linux port to OMAP based HTC Smartphones Hurricane, Tornado and Typhoon</a>,
* <a href="https://www.muru.com/linux/omap/">Muru.com Linux OMAP tree</a>,
* <a href="http://elinux.org/Android_on_OMAP#What_is_Android_.28not.29">eLinux.org - Android on OMAP</a>,
* <a href="http://focus.ti.com/general/docs/wtbu/wtbusplashcontent.tsp?templateId=6123&amp;contentId=4750">TI: Linux® Community for OMAP&#x2122; Processors: Software Downloads</a>.
