+++
title = "Weekly GNU-like Mobile Linux Update (11/2023): Pocket Reforms and Other Improvements"
draft = false
date = "2023-03-19T22:10:00Z"
[taxonomies]
tags = ["Manjaro", "MNT Pocket Reform","libadwaita","Convergence", "PINE64 Community Q&A",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Libadwaita 1.3, new unofficial Ubuntu Touch builds, Matrix support in Chatty, improvements to GNOME Software on Alpine and more!
<!-- more -->
_Commentary in italics._

### Hardware
- [MNT Pocket Reform | Crowd Supply](https://www.crowdsupply.com/mnt/pocket-reform) - it's not only available for funding, it's already made the threshold!

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#87 Editable Shortcuts](https://thisweek.gnome.org/posts/2023/03/twig-87/)
- alexm: [Libadwaita 1.3](https://blogs.gnome.org/alexm/2023/03/17/libadwaita-1-3/)
- tchx84: [Portfolio 0.9.15](https://blogs.gnome.org/tchx84/2023/03/15/portfolio-0-9-15/)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: “More Wayland fixes”](https://pointieststick.com/2023/03/17/this-week-in-kde-more-wayland-fixes/)
- KDE Announcements: [KDE Plasma 5.27.3, Bugfix Release for March](https://kde.org/announcements/plasma/5/5.27.3/)
- Theophile: [My experience taking part in Season of KDE](https://theophile.gilgien.net/posts/blog-sok/)
- KDE Eco: [Adapting Standard Usage Scenario Scripts For KDE Applications: My Journey As A Season Of KDE Mentee](https://eco.kde.org/blog/2023-03-15-sok23-kde-eco_be/)

#### Ubuntu Touch
- [Releases · Oren Klopfer / PinePhone + PinePhone Pro UBTouch Image Builder · GitLab](https://gitlab.com/ook37/pinephone-pro-debos/-/releases) _Focal for both, PinePhone and PinePhone Pro!_

#### Distributions
- Breaking updates in pmOS edge: [Alpine's ARM builders are temporarily unavailable](https://postmarketos.org/edge/2023/03/13/alpine-arm-builders-gone/)
- Manjaro PinePhone Phosh: [Beta 30](https://github.com/manjaro-pinephone/phosh/releases/tag/beta30)
- Phoronix: [Debian 12 "Bookworm" Enters Its Hard Freeze](https://www.phoronix.com/news/Debian-12-Hard-Freeze)

#### Non-Linux
- Lup Yuen: [GitHub - lupyuen/pinephone-nuttx-usb: PinePhone USB Driver for Apache NuttX RTOS](https://github.com/lupyuen/pinephone-nuttx-usb#power-on-the-usb-controller)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-03-17](https://matrix.org/blog/2023/03/17/this-week-in-matrix-2023-03-17)
- Matrix.org: [The DMA Stakeholder Workshop: Interoperability between messaging services](https://matrix.org/blog/2023/03/15/the-dma-stakeholder-workshop-interoperability-between-messaging-services)

### Worth noting
- - feborg: [Register for Linux App Summit 2023!](https://feborg.es/register-for-linux-app-summit-2023/) _I'll likely will only be able to attend virtually, but you should totally go if you can!_
- [Clayton: "Hello again #postmarketOS edge users! [...] Quite often I was burning through my mobile data cap *much* quicker than normal, because a lot of traffic was being routed over mobile data... despite being connected to a perfectly fine, unmetered wifi network.…"](https://freeradical.zone/@craftyguy/110014942107339916)
- [LinuxPhoneApps.org: "Right now, we're (cautiously) working towards moving to individual markdown files as "source of truth" for app listings (instead of .csv). Many things need to be considered. Please chime in!  …"](https://fosstodon.org/@linuxphoneapps@linuxrocks.online/110023687517501631) _Especially regarding snaps and ratings!_

### Worth reading
- Purism: [Toward Matrix support in Chats – Part 2](https://puri.sm/posts/toward-matrix-support-in-chats-part-2/)
- Linux Phone Guides: [Librem 5: A to Z](https://linuxphoneguides.ovh/index.php/2023/03/18/librem-5-a-to-z/)
- fossphones.com: [Linux Phone News - March 17, 2023](https://fossphones.com/03-17-23.html)
- Pablo Yoyoista: [GNOME Software and postmarketOS: a bumpy road – GNOME adventures in mobile](https://blogs.gnome.org/pabloyoyoista/2023/03/05/gs-and-pmos-a-bumpy-road/) _Great work!_
- Laoblog: [Pinephone review: Ubuntu Touch](https://blog.libero.it/Laoblog2/16586542.html) _Nitpick: No need to be in denial and stick to xenial, see above!_
- Purism: [Desktop Apps on the Lapdock](https://puri.sm/posts/desktop-apps-on-the-lapdock/)
- Hamblingreen: [Nexdock First Impressions](https://hamblingreen.com/2023/03/11/nexdock-first-impressions.html)

### Worth listening
- postmarketOS Podcast: [#29 GNOME Software + apk, get your patches merged, pbsplash](https://cast.postmarketos.org/episode/29-GNOME-Software-apk-get-your-patches-merged-pbsplash/)

### Worth watching
- Purism: [Desktop Apps on the Lapdock](https://www.youtube.com/watch?v=5xny14NajI0)
- Canal do Lukita: [Linux Phone - Ubuntu Touch - The best mobile OS](https://www.youtube.com/watch?v=HEq3BXlr7ZE)
- CyberPunked: [Droidian (Mobian / Debian for Android) - Google Pixel 3a (2023-03-15)](https://www.youtube.com/watch?v=ObKTkecPcqA)
- Sailfish official: [Xperia 5 droidian os and waydroid 11](https://www.youtube.com/watch?v=rKJLQ4fQdKc)
- Scientific Perspective: [postmarketOS | Linux 6.2.0](https://www.youtube.com/watch?v=iP8D_xW3XhM)
- emde: [Ephemeral messaging](https://www.youtube.com/watch?v=mH3LuE05lTM) _Impressive!_
- Eric Lovejoy: [opencv4 on pinephone pro drawing circles on level and perpendicular using iio bus accelerometer.](https://www.youtube.com/watch?v=o9j6mGfayxY)
- PINE64: [PINE64 Quarterly Community Q&A](https://tilvids.com/w/6zPrN76odWPdsj94wCJeUf), [YouTube](https://www.youtube.com/watch?v=N9_nOjAtsmw)

### Thanks
Huge thanks again to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

