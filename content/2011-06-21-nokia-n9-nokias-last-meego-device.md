+++
title = "Nokia N9 - Nokias last MeeGo device"
aliases = ["2011/06/nokia-n9-nokias-last-meego-device.html"]
date = "2011-06-21T06:45:00Z"
layout = "post"
[taxonomies]
tags = ["announcement", "communicasia 2011", "Nokia", "Nokia N9", "Nokia N950"]
categories = ["hardware", "commentary"]
authors = ["peter"]
+++
Nokia unleashed what is believed to be their last MeeGo (1.2 &#8220;Harmattan&#8221;) device at CommunicAsia today&mdash;as rumored a very long time before, it will be called &#8220;N9&#8221; and appears to be aimed at everybody, not just developers. Accelerated by a TI OMAP 3630 SoC (as in the Motorola Milestone2 and the Palm Pre2) the N9 reminds us more of late last years phones, than of this years dual core packet giants of the competition. The device, which is packed with 1024MB of Ram, features a nice 854x480 AMOLED screen with a capacitive touch layer on top and an 8 megapixel Carl Zeiss camera capable of recording HD video, looks awesome and most likely wasn't intended to be for developers only while designed&mdash;a beautiful, sleek mix of angles and curves, a piece of design, that can surely take on the iPhone in the &#8220;looks&#8221; department. Don't believe me? Have a glimpse at Nokias promo clip for the N9:

If you can't get enough of this buttonless beauty, make sure to check out what one can describe as a product page of the N9: <a href="http://swipe.nokia.com/">swipe.nokia.com</a> which has a name that really describes what how to use this device without buttons: Swiping. And what's great about this: It's a different type of swiping compared to HP webOS or other platforms that use swiping or gestures for UI use&mdash;yet it appears to be a pleasure to use.

Nokia also announced <a href="http://conversations.nokia.com/2011/06/21/nokia-steps-it-up-a-gear-with-new-accessories/?cid=ncomblogs-fw-scl-na-bsm-na-twitter-g0-en-na&amp;sf1671724=1">a set of accessoires</a> to go with the NFC enabled N9, and you should check out the <a href="http://europe.nokia.com/find-products/devices/nokia-n9-00">classic product page</a> (which suggests that the N9 will only be sold in certain parts of Europe and Asia, excluding the US (which has never been Nokias turf) and Germany) and <a href="http://conversations.nokia.com/2011/06/21/video-diving-into-the-nokia-n9-ui-and-specs/?cid=ncomblogs-fw-scl-na-bsm-na-twitter-g0-en-na&amp;sf1671725=1">spec and UI informations too</a>, if you are interested into this kind of information.

Last, but not least there seems to be another MeeGo device, only aimed at developers,packed with a QWERTY keyboard and thus immensly exiciting for me. <a href="http://www.meegoexperts.com/2011/06/confirmation-meego-nokia-n950-specifications-aimed-developers/">Check out what MeeGoExperts.com have on this device.</a>

I will be back for more thoughts on these announcements this evening (Berlin time).
